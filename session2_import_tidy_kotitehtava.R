#' ---
#' title: "Kotitehtävä 2"
#' author: lksalo
#' output:
#'   html_document:
#'  #   toc: true
#'  #   toc_float: true
#'     number_sections: yes
#'     code_folding: show
#' ---

#' [Linkki kotitehtävän lähdekoodiin gitlab:ssa](https://gitlab.com/utur2016/content/raw/master/session2_import_tidy_kotitehtava.R)

#+ setup, include=FALSE
library(knitr)
opts_chunk$set(list(echo=TRUE,eval=FALSE,cache=FALSE,warning=TRUE,message=TRUE))


#' # Tiedostojärjestelmäfunktiot
#' 
#' R:ssä on funktiot käyttöjärjestelmän tiedostojärjestelmän käyttöön, kuten tiedostojen luomiseen (`file.create()`) 
#' kansioiden luomiseen (`dir.create()`).
#' 
#' **Kysymys:** *Millä komennolla luot nykyisen työhakemistoon kansion `aineistot`?*
#+ vastaus11
dir.create("./aineistot")

#' **Kysymys:** *Millä komennolla luot kansioon `aineistot` tiedoston 'muistiinpanot.txt'?*
#+ vastaus12
file.create("./aineistot/muistiinpanot.txt")

#' **Kysymys:** *Millä komennolla kopioit kansiossa `aineistot` olevan tiedoston 'muistiinpanot.txt' samaan kansioon nimellä 'muistiinpanot.md'?*
#+ vastaus13
dir.create("./muistiinpanot.md")
file.copy(from = "./aineistot/muistiinpanot.txt", to = "./muistiinpanot.md")

#' # Datarakenteiden perusteet
#' 
#' Tällä kurssilla käsittelemme R:n kymmenistä ellei sadoista datarakenteista ainoastaan vektoreita ja data.frameja (tibblejä)
#' 
#' **Kysymys:** Millä komennolla luot numeerisen vektorin nimeltä `numerot`, jossa on kokonaisluvut väliltä 10 - 20?
#+ vastaus21
numerot <- c(10:20)

#' **Kysymys:** Millä komennolla luot kirjainvektorin `pohjoismaat` (character vector), 
#' jonka elementteinä ovat pohjoismaiden nimet suomeksi kirjoitettuna aakkosjärjestyksessä?
#+ vastaus22
pohjoismaat <- c("Islanti", "Norja", "Ruotsi", "Suomi", "Tanska")

#' **Kysymys:** Miten luot pienen data.framen, jossa sarakkeiden niminä ovat `etunimi`, `sukunimi`,`puolue` ja `titteli` ja 
#' kolmella rivillä hallituspuolueiden puheenjohtajat, joista sarakkeissa vaaditut tiedot. `titteli` viittaa siis 
#' henkilön ministeripestiin hallituksessa.
#+ vastaus23
etunimi <- c("Juha", "Timo", "Petteri")
sukunimi <- c("Sipilä", "Soini", "Orpo")
puolue <- c("Kekusta", "PS", "Kokoomus")
titteli <- c("Pääministeri", "Ulkoasiain- ja eurooppaministeri", "Valtiovarainministeri")
df = data.frame(etunimi, sukunimi, puolue, titteli, stringsAsFactors = FALSE)

#' # Datatiedoston lataaminen verkosta ja tallentaminen koneelle
#' 
#' Käyttämämme data "[Wages and Education of Young Males](https://vincentarelbundock.github.io/Rdatasets/doc/plm/Males.html)" 
#' löytyy osoitteesta https://vincentarelbundock.github.io/Rdatasets/csv/plm/Males.csv
#' 
#' **Kysymys:** *Miten tallennan ko. tiedoston nimellä `males.csv` kansioon `aineistot`?*
#+ vastaus31
download.file("https://vincentarelbundock.github.io/Rdatasets/csv/plm/Males.csv", destfile="./aineistot/Males.csv")


#' # Datatiedoston tuominen R:ään
#' 
#' Paikallisessa kansiossa olevan **tekstimuotoisen** datan tuomiseen käytetään 
#' useimmiten `read.table()`-funktiota tai mikäli kyseessä on pilkuilla erotettu .csv tiedosto
#' `read.csv()`-funktiota. Funktio tarvii argumenteikseen polun tiedostoon `path=...`. Lisäksi usein 
#' määritellään lisäargumentit kuten `header=TRUE/FALSE`, `stringsAsFactors=TRUE/FALSE` ja 
#' toisinaan `FileEncoding="Latin1"` jos käytettävä tiedosto on windowsissa luotu ääkkösiä sisältävä data.
#' 
#' **Kysymys:** *Miten tuot edellisessa vaiheessa tallentamasi tiedoston R:ään funktiolla `read.csv()`, jotta
#' objektin nimeksi tulee `malesdata`?*
#+ vastaus41
malesdata <- read.csv("./aineistot/Males.csv")

#' # Datan tarkastelu R:ssä
#' 
#' R:ssä on erilaisia funktioita datan kuvailuun kuten `str()` tai `summary()`.
#' 
#' **Kysymys:** *Miten saat konsoliin/päätteeseen näkyville äsken lataamasi `malesdata` aineiston kuusi ensimmäistä riviä?*
#+ vastaus51
head(malesdata, 6)

#' **Kysymys:** *Miten saat konsoliin/päätteeseen näkyville äsken lataamasi `malesdata` aineiston muuttujien luokat (class)?*
#+ vastaus52
sapply(malesdata, class)

#' **Kysymys:** *Mikä `malesdata` aineiston tapausten syntymävuoden keskiarvo (muuttuja `year`)?*
#+ vastaus53
birthmean <- mean(malesdata$year)
birthmean # vastaus: 1983.5

#' **Kysymys:** *Montako uniikkia ammattia (muuttuja `occupation`) on datassa  `malesdata`?*
#+ vastaus54
lapply(malesdata, function(x)length(unique(x))) # 9

#' **Kysymys:** *Mikä osuus `malesdata` aineiston tapauksista on naimisissa (muuttuja `married`)?*
#+ vastaus55
100*prop.table(table(malesdata$married))

#' # Datan siivoaminen R:ssä
#' 
#' Datan siivoaminen korostuu sotkuisten "tosielämän" datojen kanssa työskenneltäessä. 
#' Meidän data on valmiiksi käsitelty tutkimusdata, jolloin siivoamisen tarve on pienempi. 
#' Kuitenkin datassa jossain muuttujissa välilyönnit on korvattu alaviivoilla.
#' 
#' **Kysymys:** *Miten korvaat muuttujien `industry` ja `occupation` arvojen alaviivat välilyönneiksi?*
#+ vastaus61
malesdata$industry <- gsub("_", " ", malesdata$industry)
malesdata$occupation <- gsub("_", " ", malesdata$occupation)

#' **Kysymys:** *Miten muutat muuttujien `industry` ja `occupation` kaikki kirjaimet pieniksi?*
#+ vastaus62
malesdata$industry <- tolower(malesdata$industry)
malesdata$occupation <- tolower(malesdata$occupation)

#' # Datan muokkaaminen R:ssä
#' 
#' Toisinaan tarvit datasta ryhmätason yhteenvetotietoja. 
#' `dplyr`-paketin `group_by`- ja `summarise` -funktiot ovat näppäriä tässä.
#' 
#' **Kysymys:** *Kuinka saan ammattiryhmittäiset (muuttuja `occupation`) vastaajien määrät 
#' sekä ammattiryhmittäiset koulutusvuosien (`school´) keskiarvon? 
#' Tee tämä yhteenvetodata uudeksi objektiksi `malesdatasum` (käytetään sitä myöhemmin)*
#+ vastaus71
install.packages("dplyr")
library(dplyr)
malesdatasum <- malesdata %>% group_by(occupation) %>% summarise(koulu_ka = mean(school), occup_maara = n()) 

#' **Kysymys:** Ryhmittele `malesdata` siten että saat eri teollisuudenalojen (`industry`)
#' vuosittaiset palkan keskiarvot. Käytä sitten tidyr-paketin spread-funktiota ja luo datasta leveä
#' versio, joka näyttää tältä
#' 
#' | industry     | 1980 | 1981 | 1982 | ...
#' | ------------ | ---- | ---- | ---- | ...
#' | Agricultural | 1.20 | 1.16 | 1.32 | ...
#' | Construction | 1.33 | 1.54 | 1.58 | ...      
#' 
#+ vastaus72
install.packages("tidyr")
library(tidyr)
malesdata %>% dplyr::group_by(year, industry) %>% dplyr::summarise(palkka_ka = mean(wage)) -> malesdata
tidyr::spread(malesdata, key = "year", value = "palkka_ka")

#' # Datan visualisoiminen R:ssä
#' 
#' Aikaisemmassa tehtävässä laadit datan nimeltä `malesdatasum`.
#' 
#' **Kysymys:** *Miten piirrät `ggplot2`-kirjastolla pylväsdiagrammin, 
#' jossa kullekin ammattiryhmälle on tolppansa väritetty vastaajien määrän mukaan ja 
#' jossa kunkin ammattiryhmän tolpan pituus vastaa koulutusvuosien keskiarvoa?*
#+ vastaus81
install.packages("ggplot2")
library(ggplot2)
ggplot(malesdatasum, aes(x=occupation, y=koulu_ka)) + 
  geom_bar(aes(fill=occup_maara),
           stat="identity",
           position=position_dodge())

#' **Kysymys:** *Miten piirrät `ggplot2`-kirjastolla viivadiagrammin kustakin vastaajasta,
#' jossa x-akselilla on kokemus (`exper`) ja y-akselilla palkka (`wage`) ja viivan värin määrä se kuuluuko liittoon (`union`)? 
#' (Muista määritellä group-parametriksi tapausten id (`nr`))
#+ vastaus82
ggplot(malesdata, aes(x=exper, y=wage, colour=union)) +
  geom_line(aes(group=nr))

#' **Kysymys:** *Jatka edellisen kuvaa niin, jaat vastaajat paneeleihin teollisuudenalan (`industry`) mukaan ja 
#' lisäät viivaan läpinäkyvyyttä .5 verran?*
#+ vastaus83
ggplot(malesdata, aes(x=exper, y=wage, colour=union)) +
  geom_line(aes(group=nr), stat="identity", alpha=0.5) +
  facet_grid(~industry) + facet_wrap(~industry)

#´# Datan lukeminen R:ään

#' **Kysymys:** *Maailmanpankki ylläpitää ekseliä, johon on koottu taloudellista eriarvoisuutta kuvaavan gini-indeksin
#' arvoja eri maista eri tutkimusprojekteista. Ekseli sijaitsee täällä: http://siteresources.worldbank.org/INTRES/Resources/469232-1107449512766/allginis_2013.xls - 
#' miten luet sen R:ään?*
#+ vastaus91
download.file("http://siteresources.worldbank.org/INTRES/Resources/469232-1107449512766/allginis_2013.xls", destfile = "./allginis_2013.xls")

#' **Kysymys:** *Luennoilla vilkaisimme Tilastokeskuksen rajapintaan. Miten saan pxweb-paketilla Tilastokeskuksesta
#' suomenkielisen taulukon kuntien vuodne 2016 avainluvuista siivottuna Akaan kunnasta Alle 15-vuotiaiden osuuden väestöstä*
#+ vastaus94
install.packages("pxweb")
tkdata <- pxweb::interactive_pxweb()
vaesto_osuus_data <- 
  get_pxweb_data(url = "http://pxnet2.stat.fi/PXWeb/api/v1/fi/Kuntien_avainluvut/2016/kuntien_avainluvut_2016_viimeisin.px",
                 dims = list("Alue 2016" = c('020'),
                             Tiedot = c('M391')),
                 clean = TRUE)

#' **Kysymys:** *Missä tiedostomuodossa sinun käyttämäsi datat enimmäkseen ovat?*
#+ vastaus95
csv

#' **Kysymys:** *Kenen tiedontuottajan datoihin haluaisin päästä ohjelmallisesti käsiksi?*
#+ vastaus96
Tilastokeskusen ja Eurostatin lisäksi: Kela, ETK, THL, Tietoarkisto, European Social Survey, OECD, SPR
some yms; Facebook, Twitter, Whatsapp, Youtube, Reddit

#' # Lue
#' 
#' - Rstudion tuoreen yhteistöblogin eka postaus, jossa haastattelussa Rstudion CEO J.J Allaire <https://www.rstudio.com/2016/10/12/interview-with-j-j-allaire/> Hyvä kuvaus siitä, mikä Rstudio on ja mitä avoimen lähdekoodin tutkimusohjelmistot ovat vuonna 2016 
#' 
#' # Tutustu
#' 
#' - R Data Import/Export <https://cran.r-project.org/doc/manuals/r-devel/R-data.html> *This is a guide to importing and exporting data to and from R.*
#' - R for Data Science: Data import <http://r4ds.had.co.nz/data-import.html>
#' 
#' # Katso
#' 
#' - Getting Data into R <https://vimeo.com/130548869> 
