---
title: "Oheismateriaalit"
output:
  html_document:
    toc: true
    toc_float: true
    number_sections: yes
    code_folding: show
---

Kokoan tänne teemoittain tärkeitä resursseja


# Avun hakeminen!!

Avoimen lähdekoodin onjelmointikielen opiskelu ja käyttö on yhtä paljon *googlaamisen* opiskelua kun se on itse ohjelmointia. Hyvä ja aikaansaava ohjelmoija on ennen kaikkea hyvä muodostamaan täsmällisiä hakulausekkeita googleen.

## "Virallinen" dokumentaatio

Virallinen dokumentaatio on täsmällisin, mutta alussa melko vaikeaselkoista. Siihen on hyvä aina palata kun tiedot lisääntyvät.

- Getting Help with R: <https://www.r-project.org/help.html> - **tutustu tähän huolella**
- Official FAQ and HOWTO Documents: <https://cran.r-project.org/faqs.html>
- The R Manuals <https://cran.r-project.org/manuals.html>
    - An Introduction to R [html](https://cran.r-project.org/doc/manuals/r-release/R-intro.html), [pdf](https://cran.r-project.org/doc/manuals/r-release/R-intro.pdf)
- sähköpostilistat <https://www.r-project.org/mail.html> - yleisiä ja tiettyihin teemoihin keskittyviä sähköpostilistoja. R-help listalle tulee kymmeniä viestejä päivässä.

Yksi hyvä tapa tutustua R-projektin rakenteeseen on käydä läpi temaattiset "Task View":t <https://cran.r-project.org/web/views/> kuten [CRAN Task View: Statistics for the Social Sciences](https://cran.r-project.org/web/views/SocialSciences.html)

Selaamalla [R-journaalia](https://journal.r-project.org/) pysyt ajan tasalla uusista *vakavasti* otettavista paketeista, jotka ovat päässeet CRAN:iin, mutta joita kuvaavat paperit ovat myös läpäisseet vertaisarvioinnin.

## Stack Overflow

SO on vilkas ohjelmointiin keskittyvä kysymys-vastaus -sivusto, jossa käydään paljon R:n liittyvää keskustelua. Katso:

- yleinen johdanto/linkkilista R-resursseihin <http://stackoverflow.com/tags/r/info>
- uusimmat tagilla `r` olevat kysymykset: <http://stackoverflow.com/questions/tagged/r>
- syksyn 2016 uutuus **Documentations** <http://stackoverflow.com/documentation/r/topics> - suosittelen!

## Rstudion lunttilaput (Cheat-sheets)

- Linkit kaikkiin <https://www.rstudio.com/resources/cheatsheets/>
    - Base-R <https://www.rstudio.com/wp-content/uploads/2016/10/r-cheat-sheet-3.pdf>
    - Rstudio IDE <https://www.rstudio.com/wp-content/uploads/2016/01/rstudio-IDE-cheatsheet.pdf>
    - Data Wrangling with tidyr and dplyr <https://www.rstudio.com/wp-content/uploads/2015/02/data-wrangling-cheatsheet.pdf> 
    - Data Visualization with ggplot2 <https://www.rstudio.com/wp-content/uploads/2015/12/ggplot2-cheatsheet-2.0.pdf>
    - Regular expressions <https://www.rstudio.com/wp-content/uploads/2016/09/RegExCheatsheet.pdf>
    - R Markdown <https://www.rstudio.com/wp-content/uploads/2016/03/rmarkdown-cheatsheet-2.0.pdf>
    - R Markdown Reference Guide <https://www.rstudio.com/wp-content/uploads/2015/03/rmarkdown-reference.pdf>
- [olennaisimmat yhdessä pdf:ssä](http://courses.markuskainu.fi/utur2016/kuviot/r-lunttilaput20161026.pdf)

# R:n perusteet

## Lue/tutustu

- An introduction to computational data analysis. Stat 133 - UC Berkeley <http://gastonsanchez.com/stat133/>
    - <http://www.stat.berkeley.edu/~s133/all2011.pdf>
- [American Statistical Association Undergraduate Guidelines Workgroup. 2014. 2014 curriculum guidelines for undergraduate programs in statistical science. Alexandria, VA: American Statistical Association](http://www.amstat.org/education/pdfs/guidelines2014-11-15.pdf)
- <http://www.stat.berkeley.edu/~s133/all2011.pdf>
- Data wrangling, exploration, and analysis with R - UBC STAT 545A and 547M <http://stat545.com/>
- Software carpenty <http://software-carpentry.org/>
- Data carpenty <http://www.datacarpentry.org/>
* <http://happygitwithr.com>
* <https://beta.rstudioconnect.com/jennybc/happy-git-with-r/>
* <https://bookdown.org/connect/#/documents> *I never see any evidence that I've done anything here. Why is that?*
* <https://bookdown.org/jennybc/happy-git-with-r/> *I hope nothing is here!*
* <https://bookdown.org>
* Bookdown: Authoring Books with R Markdown (example and documentation)
    - rendered: <https://bookdown.org/yihui/bookdown/>
    - source: <https://github.com/rstudio/bookdown/tree/master/inst/examples>
* Official bookdown minimal example demo
    - rendered: <https://bookdown.org/yihui/bookdown-demo/>
    - source: <https://github.com/rstudio/bookdown-demo>
* R for data science
    - rendered: <http://r4ds.had.co.nz>
    - source: <https://github.com/hadley/r4ds>
* Efficient R
    - rendered: <https://csgillespie.github.io/efficientR/>
    - source: <https://github.com/csgillespie/efficientR>
* Statistical Programming Methods Textbook for STAT 385 at UIUC
    - rendered, custom domain?: <http://spm.thecoatlessprofessor.com>
    - autodeploys to gh pages via travis: <https://coatless.github.io/statprogramming>
    - source: <https://github.com/coatless/statprogramming>
* Time Series is Great!
    - source: <https://github.com/coatless/timeseriesisgreat>
    - autodeploys to gh pages via travis: <https://coatless.github.io/timeseriesisgreat>    
* Applied Statistics Course Textbook
    - source: <https://github.com/daviddalpiaz/appliedstats>
    - autodeploys to gh pages via travis: <https://daviddalpiaz.github.io/appliedstats>
    - gh-pages branch: <https://github.com/daviddalpiaz/appliedstats/commits/gh-pages>
    - Auto-output commit example: <https://github.com/daviddalpiaz/appliedstats/commit/0cfdc52e896f719a93d833e16dc5a1ba9095b563>
    - Code commit example: <https://github.com/daviddalpiaz/appliedstats/commit/a240bd6151d6533b2676833a2bf8231fd5fd3e41>
* Mastering DFS Analytics
    - source: <https://github.com/znmeb/mastering-dfs-analytics-bookdown>
    - rendered, custom domain: <https://www.masteringdfsanalytics.com>

- [Open research methods in computational social sciences and humanities: introducing R (Kainu 2014)](https://digihist.se/5-metoder-inom-digital-historia/fordjupning-open-research-methods-in-computational-social-sciences-and-humanities-introducing-r/)




## Katso

- Deborah Nolan: Statistical Thinking in a Data Science Course. UseR2016 <https://channel9.msdn.com/Events/useR-international-R-User-conference/useR2016/Statistical-Thinking-in-a-Data-Science-Course>
- Mine Cetinkaya-Rundel: A first-year undergraduate data science course. UseR2016 <https://channel9.msdn.com/events/useR-international-R-User-conference/useR2016/A-first-year-undergraduate-data-science-course>
- Michael Andrew Levy: Teaching R to 200 people in a week. UseR2016 <https://channel9.msdn.com/Events/useR-international-R-User-conference/useR2016/Teaching-R-to-200-people-in-a-week>


## Opettele

- - <https://www.rstudio.com/online-learning/>


# Datan tuominen

## Lue

- R Data Import/Export <https://cran.r-project.org/doc/manuals/r-devel/R-data.html> *This is a guide to importing and exporting data to and from R.*
- - R for Data Science: Data import <http://r4ds.had.co.nz/data-import.html>

## Katso

- Getting Data into R <https://vimeo.com/130548869> 0.00 -> 


## Paketteja datan tuomiseen

These packages help you import data into R and save data.

* [feather](https://blog.rstudio.org/2016/03/29/feather/) - a fast, lightweight file format used by both R and Python
* [readr](https://blog.rstudio.org/2015/10/28/readr-0-2-0/) - reads tabular data
* [readxl](https://blog.rstudio.org/2015/04/15/readxl-0-1-0/) - reads Microsoft Excel spreadsheets
* [openxlsx](https://github.com/awalker89/openxlsx) - reads Microsoft Excel spreadsheets
* [googlesheets](https://github.com/jennybc/googlesheets) - reads Google spreadsheets
* [haven](https://blog.rstudio.org/2015/03/04/haven-0-1-0/) - reads SAS, SPSS, and Stata files
* [httr](https://blog.rstudio.org/2016/02/02/httr-1-1-0/) - reads data from web APIs
* [rvest](https://blog.rstudio.org/2014/11/24/rvest-easy-web-scraping-with-r/) - scrapes data from web pages
* [xml2](https://github.com/hadley/xml2) - reads HTML and XML data
* [webreadr](https://cran.r-project.org/web/packages/webreadr/vignettes/Introduction.html) - reads common web log formats
* [DBI](https://github.com/rstats-db/DBI) - a universal interface to database management systems (DBMS)
    + [RMySQL](https://github.com/rstats-db/RMySQL) - MySQL driver for DBI
    + [RPostgres](https://github.com/rstats-db/RPostgres) - Postgres driver for DBI
    + [RSQLite](https://github.com/rstats-db/RSQLite) - SQlite driver for DBI
    + [bigrquery](https://github.com/rstats-db/bigrquery) - Google BigQuery driver for DBI
* [PivotalR](https://github.com/pivotalsoftware/PivotalR) - reads data from and interfaces with [Postgres](http://www.postgresql.org), [Greenplum](http://greenplum.org), and [HAWQ](http://hawq.incubator.apache.org) 
* [dplyr](https://github.com/hadley/dplyr) - contains an interface to common databases
* [data.table](https://github.com/Rdatatable/data.table) - `fread()` for fast table reading
* [git2r](https://github.com/ropensci/git2r) - tools to access git repositories


## Datapaketteja

These packages contain data sets to use as training data or toy examples.

* [babynames](https://github.com/hadley/babynames) - Names given to US babies 1880-2014
* [neiss](https://github.com/hadley/neiss) - sample of all accidents reported to US emergency rooms 2009-2014
* [yrbss](https://github.com/hadley/yrbss) - Youth Risk Behaviour Surveillance System data from 1991 to 2013
* [nycflights13](https://github.com/hadley/nycflights13) - all out-bound flights from NYC in 2013
* [hflights](https://github.com/hadley/hflights) - flights departing Houston in 2011
* [USAboundaries](https://github.com/ropensci/USAboundaries) - Historical and Contemporary Boundaries of the United States of America
* [rworldmap](https://github.com/AndySouth/rworldmap) - country border data
* [usdanutrients](https://github.com/hadley/usdanutrients) - USDA nutrient database
* [fueleconomy](https://github.com/hadley/fueleconomy) - EPA fuel economy data
* [nasaweather](https://github.com/hadley/nasaweather) - geographic and atmospheric measures on a very coarse 24 by 24 grid covering Central America
* [mexico-mortality](https://github.com/hadley/mexico-mortality) - deaths in Mexico
* [data-movies](https://github.com/hadley/data-movies) and [ggplotmovies](https://cran.r-project.org/web/packages/ggplot2movies/) - data from the Internet Movie Database (IMDB)
* [pop-flows](https://github.com/hadley/pop-flows) - Population flows around the USA in 2008
* [data-housing-crisis](https://github.com/hadley/data-housing-crisis) - Clean data related to the 2008 US housing crisis
* [gun-sales](https://github.com/NYTimes/gunsales) - Statistical analysis of monthly background checks of gun purchases from NY times
* [stationaRy](https://github.com/rich-iannone/stationaRy) - hourly meteorological data from one of thousands of global stations
* [gapminder](https://github.com/jennybc/gapminder) - Excerpt from the Gapminder data
* [janeaustenr](https://github.com/juliasilge/janeaustenr) - Jane Austen's Complete Novels



# Data siivoaminen


## Lukemista

- [Wickham, Hadley. 2014. ‘Tidy Data’. Journal of Statistical Software 59 (10). doi:10.18637/jss.v059.i10.
](http://vita.had.co.nz/papers/tidy-data.html)
- R for Data Science: Tidy Data <http://r4ds.had.co.nz/tidy-data.html>
- tidyr-paketin vignette: <https://cran.r-project.org/web/packages/tidyr/vignettes/tidy-data.html>
- Rstudio blog: Introducing tidyr <https://blog.rstudio.org/2014/07/22/introducing-tidyr/>
- Data Wrangling Cheat Sheet - RStudio  <https://www.rstudio.com/wp-content/uploads/2015/02/data-wrangling-cheatsheet.pdf>


## Paketteja

These packages help you wrangle your data into a form that is easy to analyze in R.

* [tidyr](https://github.com/hadley/tidyr) - tools for tidying layout of tabular data
* [dplyr](https://github.com/hadley/dplyr) - tools for joining multiple tables into a tidy data set
* [purrr](https://github.com/hadley/purrr) - tools for applying R functions to data structures, very useful when tidying
* [broom](http://varianceexplained.org/r/broom-intro/) - tools for tidying statistical models into data frames
* [zoo](https://www.google.com/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q=r%20zoo) - data structures for time series data
* [PivotalR](https://github.com/pivotalsoftware/PivotalR) - R wrappers for in-database SQL operations (i.e. join, group by)




# Datan muokkaaminen

## Paketteja

These packages help you transform your data into new types of data.

* [dplyr](https://github.com/hadley/dplyr) - a grammar of data transformation
* [magrittr](https://github.com/smbache/magrittr) - a concise syntax for calling sequences of functions
* [tibble](https://github.com/hadley/tibble) - efficient display structure for tabular data
* [stringr](https://blog.rstudio.org/2015/05/05/stringr-1-0-0/) - tools for working with strings and regular expressions
* [lubridate](https://cran.r-project.org/web/packages/lubridate/vignettes/lubridate.html) - tools for working with dates and times
* [xts](http://r-forge.r-project.org/projects/xts) - tools for time series based data
* [data.table](https://github.com/Rdatatable/data.table/wiki) - fast data manipulation
* [vtreat](https://github.com/WinVector/vtreat) - tools for pre-processing variables for predictive modeling
* [stringi](http://www.rexamine.com/resources/stringi/) - fast string processing facilities. 
* [Matrix](http://matrix.r-forge.r-project.org/) - LAPACK methods for dense and sparse matrix operations


# Datan visualisoiminen


## Paketteja

These packages help you visualize your data.

* [ggplot2](http://docs.ggplot2.org/current/) with [extensions](http://www.ggplot2-exts.org/) - a versatile system for making plots
    + [ggthemes](https://github.com/jrnold/ggthemes) - plot style themes
    + [ggmap](https://github.com/dkahle/ggmap) - maps with Google Maps, Open Street Maps, etc.
    + [ggiraph](http://davidgohel.github.io/ggiraph/introduction.html) - interactive ggplots
    + [ggstance](https://github.com/lionel-/ggstance) - horizontal versions of common plots
    + [GGally](https://github.com/ggobi/ggally) - scatterplot matrices
    + [ggalt](https://github.com/hrbrmstr/ggalt) - additional coordinate systems, geoms, etc.
    + [ggforce](https://github.com/thomasp85/ggforce) - additional geoms, etc.
    + [ggrepel](https://github.com/slowkow/ggrepel) - prevent plot labels from overlapping
    + [ggraph](https://github.com/thomasp85/ggraph) - graphs, networks, trees and more
    + [ggpmisc](https://cran.rstudio.com/web/packages/ggpmisc/) - photo-biology related extensions
    + [geomnet](https://github.com/sctyner/geomnet) - network visualization
    + [ggExtra](https://github.com/daattali/ggExtra) - marginal histograms for a plot
    + [gganimate](https://github.com/dgrtwo/gganimate) - animations
    + [plotROC](https://github.com/sachsmc/plotROC) - interactive ROC plots
    + [ggspectra](https://cran.rstudio.com/web/packages/ggspectra/) - tools for plotting light spectra
    + [ggnetwork](https://github.com/briatte/ggnetwork) - geoms to plot networks
    + [ggtech](https://github.com/ricardo-bion/ggtech) - style themes for plots
    + [ggradar](https://github.com/ricardo-bion/ggradar) - radar charts
    + [ggTimeSeries](https://github.com/Ather-Energy/ggTimeSeries) - time series visualizations
    + [ggtree](https://bioconductor.org/packages/release/bioc/html/ggtree.html) - tree visualizations
    + [ggseas](https://github.com/ellisp/ggseas) - seasonal adjustment tools
* [lattice](http://lattice.r-forge.r-project.org/) - Trellis graphics
* [rgl](https://cran.r-project.org/web/packages/rgl/vignettes/rgl.html) - interactive 3D plots
* [ggvis](http://ggvis.rstudio.com/) - versatile system for interactive graphs
* [htmlwidgets](http://www.htmlwidgets.org/) - framework for creating JavaScript widgets with R
    + [leaflet](http://rstudio.github.io/leaflet/) - Interactive maps
    + [dygraphs](http://rstudio.github.io/dygraphs) - Interactive time series plots
    + [plotly](https://plot.ly/r/) - Interactive plots
    + [rbokeh](http://hafen.github.io/rbokeh) - Interactive Bokeh plots
    + [Highcharter](http://jkunst.com/highcharter/) - Interactive Highcharts plots
    + [visNetwork](http://dataknowledge.github.io/visNetwork) - Interactive network graphs
    + [networkD3](http://christophergandrud.github.io/networkD3/) - Interative d3 network graphs
    + [d3heatmap](https://github.com/rstudio/d3heatmap) - Interactive d3 heatmaps
    + [DT](http://rstudio.github.io/DT/) - Interactive tables
    + [threejs](https://github.com/bwlewis/rthreejs) - Interactive 3d plots and globes
    + [rglwidget](http://cran.at.r-project.org/web/packages/rglwidget/index.html) - Interactive 3d plot
    + [DiagrammeR](http://rich-iannone.github.io/DiagrammeR/) - Interactive diagrams
    + [MetricsGraphics](http://hrbrmstr.github.io/metricsgraphics/) - Interactive MetricsGraphics plots
* [rCharts](http://rcharts.io/) - many interactive JavaScript visualizations
* [coefplot](http://github.com/jaredlander/coefplot) - visualizes model statistics
* [quantmod](http://www.quantmod.com/) - candlestick financial charts
* [colorspace](https://cran.r-project.org/web/packages/colorspace/vignettes/hcl-colors.pdf) - HSL based color palettes
* [viridis](https://github.com/sjmgarnier/viridis) - Matplotlib viridis color pallete for R
* [munsell](https://github.com/cwickham/munsell) - Munsell color palettes for R.
* RColorBrewer - color palettes for plots. No manual or website.
* dichromat - color-blind friendly palettes. No manual or website.
* [igraph](http://igraph.org/) - Network Analysis and Visualization
* [latticeExtra](http://latticeextra.r-forge.r-project.org/) - Extensions for lattice graphics
* [sp](https://github.com/edzer/sp/) - tools for spatial data


# Datan mallintaminen

## Lue 

- Frank E Harrell Jr: Regression Modeling Strategies (Joint Statistical Meetings Chicago 2016-07-31) <http://biostat.mc.vanderbilt.edu/tmp/course.pdf>

## Katso

- UseR2016: broom: Converting statistical models to tidy data frames
<https://channel9.msdn.com/Events/useR-international-R-User-conference/useR2016/broom-Converting-statistical-models-to-tidy-data-frames>


## Paketteja

These packages help you build models and make inferences. Often the same packages will focus on both topics.

* [car](https://r-forge.r-project.org/projects/car/) - functions from An R Companion to Applied Regression
* [Hmisc](https://github.com/harrelfe/Hmisc) - miscellaneous functions for data analysis
* [multcomp](http://multcomp.r-forge.r-project.org/) - Simultaneous Inference in General Parametric Models
* [pbkrtest](http://people.math.aau.dk/~sorenh/software/pbkrtest/) - parametric bootstrap test for linear mixed effects models 
* [mvtnorm](http://mvtnorm.r-forge.r-project.org/) - Multivariate Normal and t Distributions
* [MatrixModels](http://matrix.r-forge.r-project.org/) - Modelling with Sparse And Dense Matrices
* [SparseM](http://www.econ.uiuc.edu/~roger/research/sparse/sparse.html) - linear algebra for sparse matrices
* [lme4](https://github.com/lme4/lme4) - Linear Mixed-Effects Models using Eigen C++ library
* [broom](http://varianceexplained.org/r/broom-intro/) - tools for tidying statistical models into data frames
* [caret](http://topepo.github.io/caret/index.html) - tools for Classification And REgression Training
* [glmnet](https://web.stanford.edu/~hastie/glmnet/glmnet_alpha.html) - generalized linear models via penalized maximum likelihood
* [mosaic](http://mosaic-web.org/) - Tools for teaching mathematics, statistics, computation and modeling
* [gbm](https://github.com/gbm-developers/gbm) - gradient boosted regression models
* [xgboost](https://github.com/dmlc/xgboost) - Extreme Gradient Boosting
* [randomForest](https://www.stat.berkeley.edu/~breiman/RandomForests/) - Random Forests for Classification and Regression
* [ranger](https://github.com/imbs-hl/ranger) - a fast implementation of Random Forests
* [h2o](http://www.h2o.ai/) - parallel distributed machine learning algorithms
* [ROCR](http://rocr.bioinf.mpi-sb.mpg.de/) - plots to visualize classifier performance
* [pROC](http://web.expasy.org/pROC/) - Tools for visualizing, smoothing and comparing ROC curves
* [PivotalR](https://github.com/pivotalsoftware/PivotalR) - R wrappers for [MADlib](http://madlib.incubator.apache.org)'s parallel distributed machine learning algorithms



# Raportointi


## Paketteja

These packages help you communicate the results of data science to your audiences.

* [rmarkdown](http://rmarkdown.rstudio.com/) - easy-to-use format for reproducible reports and dynamic documents in R
* [knitr](http://yihui.name/knitr/) - embed R code within pdf and html reports
* [flexdashboard](http://rstudio.github.io/flexdashboard/) - easy-to-create dashboards based on rmarkdown
* [bookdown](https://bookdown.org/) - books and long documents built on R Markdown
* [rticles](https://github.com/rstudio/rticles) - ready to use R Markdown templates
* [tufte](http://rstudio.github.io/tufte/) - Tufte handout R Markdown template
* [DT](http://rstudio.github.io/DT/) - Interactive data tables
* [pixiedust](https://github.com/nutterb/pixiedust) - Customized tables
* [xtable](https://cran.r-project.org/web/packages/xtable/vignettes/xtableGallery.pdf) - Customized tables
* [highr](https://github.com/yihui/highr) - Syntax Highlighting for R Source Code
* [formatR](http://yihui.name/formatR/) - `tidy_source()` to format R source code
* [yaml](https://github.com/viking/r-yaml) - Methods to convert R data to YAML and back
* [pander](http://rapporter.github.io/pander/) - renders R objects into Pandoc markdown.


